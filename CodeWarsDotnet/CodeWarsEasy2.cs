﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace CodeWarsDotnet
{
    public static class CodeWarsEasy2
    {
        public static List<int> MultipleOfIndex1(List<int> xs) => xs.Where((j, i) => i != 0 && j % i == 0).ToList();

        public class Person
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public int Age { get; set; }
            public string Gender { get; set; }

            public Person(string firstName = "Jonhn", string lastName = "Doe", int age = 0, string gender = "Male")
            {
                this.FirstName = firstName;
                this.LastName = lastName;
                this.Age = age;
                this.Gender = gender;
            }

            public string SayFullName()
            {
                return $"{FirstName} {LastName}";
            }

            public static string GreetExtraTerrestrials(string raceName)
            {
                return $"Welcome to Planet Earth {raceName}";
            }
        }

        public static object Animals(int heads, int legs)
        {
            var cowLegs = 4;
            var chickenLegs = 2;
            var numberOfCows = 0;
            var numberOfChickens = 0;

            Console.WriteLine($"heads: {heads}, legs: {legs}");

            if ((heads > 999 || legs > 999) || (heads == 0 && legs != 0) || (heads != 0 && legs == 0) || (heads < 0 || legs < 0) || legs % 2 != 0)
            {
                return "No solutions";
            }

            if (heads == 0 || legs == 0)
            {
                return new int[2] { numberOfChickens, numberOfCows };
            }
            {
            }
            if (legs / cowLegs == heads)
            {
                numberOfCows = legs / cowLegs;
                return new int[2] { numberOfChickens, numberOfCows };
            }
            if (legs / chickenLegs == heads)
            {
                numberOfChickens = legs / chickenLegs;
                return new int[2] { numberOfChickens, numberOfCows };
            }

            //multiply chicken legs by number of heads to find out how many heads we are missing
            numberOfCows = (legs - (chickenLegs * heads)) / chickenLegs;

            numberOfChickens = (legs - (numberOfCows * cowLegs)) / chickenLegs;
            if (numberOfChickens < 0)
            {
                return "No solutions";
            }

            return new int[2] { numberOfChickens, numberOfCows };
        }

        public static List<int> MultipleOfIndex(List<int> xs)
        {
            var pi = Math.Pow(Math.PI, 2);
            var result = new List<int>();
            for (var i = 1; i < xs.Count; i++)
            {
                if (xs[i] % i == 0)
                {
                    result.Add(xs[i]);
                }
            }

            return result;
        }

        public static class Kata
        {
            public static string[] Websites = Enumerable.Repeat("codewars", 1000).ToArray();
        }

        public static string Stringy(int size)
        {
            var result = new List<string>()
            {
            }
                ;
            for (int i = 1; i < size; i++)
            {
                if (i % 2 == 0)
                {
                    result.Add("0");
                }
                if (i % 2 != 0)
                {
                    result.Add("1");
                }
            }

            return String.Join("", result);
        }

        public static string Stringy1(int size)
        {
            var result = new StringBuilder();
            for (var i = 1; i <= size; i++)
            {
                result.Append(i % 2);
            }
            return result.ToString();
        }

        public static string Stringy2(int size)
        {
            return String.Join("", Enumerable.Range(0, size).Select(x => (x + 1) % 2));
        }

        public static string Stringy3(int size)
        {
            return Regex.Replace(new string('1', size), "11", "10");
        }

        public static string FindNeedle(object[] haystack)
        {
            //Code goes here!
            var position = haystack.ToList().IndexOf("needle");
            return $"found the needle at position {position}";
        }

        public static string FindNeedle1(object[] haystack)
        {
            return "found the needle at position " + Array.IndexOf(haystack, "needle");
        }

        public static string Solution(string str)
        {
            var result = new StringBuilder();
            for (int i = str.Length - 1; i >= 0; i--)
            {
                result.Append(str[i]);
            }

            return result.ToString();
        }

        public static string Solution1(string str) => new string(str.Reverse().ToArray());

        public static long[] Digitize(long n)
        {
            var srtN = n.ToString();
            char[] reversed = srtN.Reverse().ToArray();
            long[] numbers = reversed.Select(i => Int64.Parse(i.ToString())).ToArray();
            return numbers;
        }

        public static long[] Digitize1(long n)
        {
            return n.ToString()
                .Reverse()
                .Select(t => Convert.ToInt64(t.ToString()))
                .ToArray();
        }

        public static int SumMul(int n, int m)
        {
            if (n > m || n == 0 || n == m)
            {
                throw new ArgumentException();
            }
            var result = Enumerable.Range(0, m).Where(i => i % n == 0).Sum();
            if (result == null)
            {
                Console.WriteLine($"n is {n}, m is {m}");
            }
            return result;
        }

        public static int[] humanYearsCatYearsDogYears(int humanYears)
        {
            // Your code here!
            var catYears = 0;
            var dogYears = 0;
            for (int i = 0; i <= humanYears; i++)
            {
                if (i == 1)
                {
                    catYears += 15;
                    dogYears += 15;
                }

                if (i == 2)
                {
                    catYears += 9;
                    dogYears += 9;
                }

                if (i > 2)
                {
                    catYears += 4;
                    dogYears += 5;
                }
            }

            return new int[] { humanYears, catYears, dogYears };
        }

        public static object[] IsVow(object[] a)
        {
            var result = new List<object>();
            var vowels = new List<char>() { 'a', 'e', 'i', 'o', 'u' };
            foreach (var o in a)
            {
                try
                {
                    var letter = Convert.ToChar(o);
                    result.Add(vowels.Contains(letter) ? letter.ToString() : o);
                }
                catch (Exception)
                {
                    result.Add(o);
                }
            }

            return result.ToArray();
        }

        public static object[] IsVow1(object[] a)
        {
            return a.Select(x => "aeiou".Contains(Convert.ToChar(x)) ? Convert.ToChar(x).ToString() : x).ToArray();
        }

        public static int FindDifference(int[] a, int[] b)
        {
            return Math.Abs(a.Aggregate((n, m) => n * m) - b.Aggregate((n, m) => n * m));
        }

        public static string StringClean(string s) => Regex.Replace(s, @"1234567890", "");

        public static string[] AddToString(string[] arrayStrings)
        {
            for (int i = 0; i < arrayStrings.Length; i++)
            {
                arrayStrings[i] = "codewars";
            }
            return arrayStrings;
        }

        public static string Well(string[] x)
        {
            var numberOfGood = x.Count(t => t == "good");

            if (numberOfGood == 0)
            {
                return "Fail!";
            }

            if (numberOfGood == 1)
            {
                return "Publish!";
            }

            return "I smell a series!";
        }
    }
}