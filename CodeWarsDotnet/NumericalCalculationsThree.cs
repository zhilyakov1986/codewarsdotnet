﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace CodeWarsDotnet
{
    public class NumericalCalculationsThree
    {
        /// <summary>
        /// returns the total surface area and volume of a box as an array
        /// </summary>
        /// <param name="w"></param>
        /// <param name="h"></param>
        /// <param name="d"></param>
        /// <returns></returns>
        public static int[] Get_size(int w, int h, int d)
        {
            var surfaceArea = 2 * (h * w) + 2 * (h * d) + 2 * (w * d);
            var volume = w * h * d;
            return new[] { surfaceArea, volume };
        }

        public static int[] MonkeyCount(int n)
        {
            var numList = new List<int>();
            for (int i = 1; i <= n; i++)
            {
                numList.Add(i);
            }

            return numList.ToArray();
        }

        public static int[] GetRange(int n) => Enumerable.Range(1, n).ToArray();

        public static int[] CountBy(int x, int n)
        {
            var numList = new List<int>();
            for (int i = 1; i <= n; i++)
            {
                numList.Add(i * x);
            }

            return numList.ToArray();
        }

        //using select with range
        public static int[] GetMultipliedRange(int x, int n) => Enumerable.Range(1, n).Select(i => i * n).ToArray();

        public static int HexToDec(string hexString)
        {
            var hexNumIsNegative = hexString.Contains('-');
            if (!hexNumIsNegative)
            {
                //return int.Parse(hexString, System.Globalization.NumberStyles.HexNumber);
                return Convert.ToInt32(hexString, 16);
            }
            else
            {
                var absHexString = hexString.Substring(hexString.IndexOf('-') + 1);
                return Convert.ToInt32(absHexString, 16) * (-1);
            }
        }

        public static int HexToDec2(string hexString)
        {
            return Convert.ToInt32(hexString.TrimStart('-'), 16) * (hexString[0] == '-' ? -1 : 1);
        }

        public static bool IsCube(double volume, double side)
        {
            if (volume <= 0 || side <= 0)
            {
                return false;
            }
            var cubeRoot = Math.Pow(volume, (double)1 / 3);
            if (Math.Abs(cubeRoot - side) <= 0.01)
            {
                return true;
            }

            return false;
        }

        public bool IsCube2(double volume, double side)
        {
            return (Math.Pow(side, 3) == volume && side > 0);
        }

        public static int StringToNumber(String str)
        {
            return int.Parse(str);
        }

        public static string NoSpaceRegex(string input) => Regex.Replace(input, @"\s+", "");

        public static string NoSpace(string input) => input.Replace(" ", "");

        public static int binToDec(string s) => Convert.ToInt32(s, 2);

        public static int[] DivisibleBy(int[] numbers, int divisor)
        {
            return numbers.Where(i => i % divisor == 0).ToArray();
        }

        /// <summary>
        //1 -> 8  (2*2*2-0*0*0)
        //2 -> 26 (3*3*3-1*1*1)
        //3 ->    (4*4*4-2*2*2)
        //4 -> 98 (5*5*5-3*3*3)
        //5 ->    (6*6*6-4*4*4)
        /// </summary>
        /// <param name="cuts"></param>
        /// <returns></returns>
        public static int CountSquares(int cuts)
        {
            //if (cuts==0)
            //{
            //    return 1;
            //}
            //return (int)Math.Pow(cuts + 1, 3) - (int) Math.Pow(cuts - 1, 3);
            return cuts == 0 ? 1 : (int)Math.Pow(cuts + 1, 3) - (int)Math.Pow(cuts - 1, 3);
        }

        public static bool BetterThanAverage(int[] ClassPoints, int YourPoints)
        {
            //Insert brain here
            return !(ClassPoints.Average() > YourPoints);
        }

        public static int SumMix(object[] x)
        {
            var listOfInt = new List<int>();
            foreach (var t in x)
            {
                if (t is string)
                {
                    int.TryParse(t.ToString(), out var number);
                    listOfInt.Add(number);

                }
                else
                {
                    listOfInt.Add((int)t);
                }
            }

            return listOfInt.Sum();
        }

        public static int SumMix2(object[] x) => x.Sum(Convert.ToInt32);

        public static int TotalPoints(string[] games)
        {
            // insert magic here
            int total = 0;
            for (int i = 0; i < games.Length; i++)
            {
                var gameSplit = games[i].Split(':');
                int.TryParse(gameSplit[0].ToString(), out var first);
                int.TryParse(gameSplit[1].ToString(), out var second);
                if (first > second)
                {
                    total += 3;
                }
                if (first == second)
                {
                    total += 1;
                }
            }
            return total;
        }
        public static int TotalPoints1(string[] games)
        {
            return games.Select(x =>
            {
                var values = x.Split(':').Select(int.Parse);

                if (values.ElementAt(0) > values.ElementAt(1))
                {
                    return 3;
                }

                return values.ElementAt(0) < values.ElementAt(1)
                    ? 0
                    : 1;
            }).Sum(x => x);
        }

        public static string AbbrevName(string name)
        {
            name = name.ToUpper();
            var split = name.Split(' ');
            return $"{split[0][0]}. {split[1][0]}";
        }
        public static string AbbrevName1(string name) => string.Join(".", name.Split(' ').Select(w => w[0])).ToUpper();

        public static IEnumerable<string> GooseFilter(IEnumerable<string> birds)
        {
            // return IEnumerable of string containing all of the strings in the input collection, except those that match strings in geese
            string[] geese = new string[] { "African", "Roman Tufted", "Toulouse", "Pilgrim", "Steinbacher" };
            var geeseSet = new HashSet<string>(geese);
            var birdSet = new HashSet<string>(birds);
            return birdSet.Except(geeseSet);
        }

        public static long StairsIn20(int[][] stairs)
        {
            var sum = 0;
            foreach (var day in stairs)
            {
                foreach (var d in day)
                {
                    sum += d;
                }
            }

            return sum;
        }
        public static long StairsIn20_1(int[][] stairs) => stairs.Sum(x => x.Sum()) * 20;

        public static string Greet(string language)
        {
            // Happy Codding :)
            var dict = new Dictionary<string, string>()
            {
                {"english", "Welcome"},
                {"czech", "Vitejte"},
                {"danish", "Velkomst"},
                {"dutch", "Welkom"},
                {"estonian", "Tere tulemast"},
                {"finnish", "Tervetuloa"},
                {"flemish", "Welgekomen"},
                {"french", "Bienvenue"},
                {"german", "Willkommen"},
                {"irish", "Failte"},
                {"italian", "Benvenuto"},
                {"latvian", "Gaidits"},
                {"lithuanian", "Laukiamas"},
                {"polish", "Witamy"},
                {"spanish", "Bienvenido"},
                {"swedish", "Valkommen"},
                {"welsh", "Croeso"}
            };
            if (dict.ContainsKey(language))
            {
                return dict[language];
            }

            return "Welcome";
        }

        public static bool IsOpposite(string s1, string s2)
        {
            if (s1.Length == 0 || s2.Length == 0 || s1.Length != s2.Length || !string.Equals(s1, s2, StringComparison.CurrentCultureIgnoreCase))
            {
                return false;
            }
            for (var i = 0; i < s1.Length; i++)
            {
                if (s1[i] == s2[i])
                {
                    return false;
                }
            }

            return true;
        }

        public static bool IsOpposite1(string s1, string s2)
            => !string.IsNullOrEmpty(s1) && s1.SequenceEqual(s2.Select(e => char.IsLower(e) ? char.ToUpper(e) : char.ToLower(e)));

        public static bool IsLockNessMonster(string sentence)
        {
            var keyPhrases = new List<string>() { "tree fiddy", "3.50", " three fifty" };
            foreach (var keyPhrase in keyPhrases)
            {
                if (sentence.Contains(keyPhrase, StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }
            }

            return false;
        }

        public static bool IsLockNessMonster1(string sentence)
        {
            return sentence.ToUpper().Contains("TREE FIDDY") || sentence.Contains("3.50");
        }

        public static bool IsLockNessMonster2(string sentence)
            => Regex.IsMatch(sentence, @"tree fiddy|3\.50", RegexOptions.IgnoreCase);

        public static string AreYouPlayingBanjo(string name)
        {
            return name.StartsWith("r", StringComparison.CurrentCultureIgnoreCase)
                ? $"{name} plays banjo"
                : $"{name} does not play banjo";
        }

        public static string RemoveExclamationMarks(string s) => Regex.Replace(s, @"!", "");
        public static string RemoveExclamationMarks1(string s) => s.Replace("!", "");

        public static List<int> ReverseList(List<int> list)
        {
            // Return a new list with its elements in reverse order compared to the input list
            // Do not mutate the original list!
            var listCopy = new List<int>(list);
            listCopy.Reverse();
            return listCopy;
        }

        public static List<int> ReverseList1(List<int> list) => list.AsEnumerable().Reverse().ToList();

        public static List<int> ReverseList2(List<int> list)
        {
            return Enumerable.Reverse(list).ToList();
        }

        //.Where((item, index) =>
        public static object[] RemoveEveryOther1(object[] arr) => arr.Where((t, i) => i % 2 != 0).ToArray();

        public static object[] RemoveEveryOther(object[] arr)
        {
            var everyOther = new List<object>();
            for (int i = 0; i < arr.Length; i++)
            {
                if (i % 2 != 0)
                {
                    everyOther.Add(arr[i]);
                }
            }

            return everyOther.ToArray();
        }

        public static int GetAge(string inputString)
        {
            // return correct age (int). Happy coding :)
            return int.Parse(inputString.First().ToString());
        }

        public static int[] CountPositivesSumNegatives(int[] input)
        {
            try
            {
                var positive = input.Where(i => i > 0);
                var negative = input.Where(i => i < 0);

                if (input == null || input.Length == 0 || !input.Any() || (!positive.Any() && !negative.Any()))
                {
                    return new int[] { };
                }
                return new[] { positive.Count(), negative.Sum() };
            }
            catch (Exception)
            {
                return new int[] { };
            }

        }
        public static int[] CountPositivesSumNegatives1(int[] input)
        {
            return (input == null || input.Length == 0) ? new int[0] : new int[] { input.Count(o => o > 0), input.Where(o => o < 0).Sum() };
        }

        public static int CountSheeps(bool[] sheeps)
        {
            return sheeps.Count(i => i);
        }
    }
}