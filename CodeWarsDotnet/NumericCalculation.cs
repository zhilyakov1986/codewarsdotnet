﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CodeWarsDotnet
{
    public class NumericCalculation
    {
        public static int Litres(double time)
        {
            var drunk = time * 0.5;
            return (int)drunk;
        }

        public static int StrCount(string str, string letter)
        {
            var count = 0;
            foreach (var symbol in str)
                if (symbol.ToString() == letter)
                    count++;

            return count;
        }

        public static int strCountLinq(string str, string letter)
        {
            return str.Count(x => x.ToString() == letter);
        }

        public static int OtherAngle(int a, int b)
        {
            return 180 - (a + b);
        }

        public static int multiply(int a, string b)
        {
            return a * int.Parse(b);
        }

        public static bool ValidParentheses(string input)
        {
            var braceCount = 0;
            foreach (var brace in input)
            {
                if (braceCount < 0) return false;

                if (brace == '(') braceCount++;

                if (brace == ')') braceCount--;
            }

            return braceCount == 0;
        }

        public static string Maskify(string cc)
        {
            const int unmaskedPartLength = 4;
            var strLength = cc.Length;
            if (strLength <= unmaskedPartLength) return cc;
            var maskedPart = strLength - unmaskedPartLength;
            var unmaskedPart = cc.Substring(strLength - unmaskedPartLength);
            var mask = new string('#', maskedPart);
            return mask + unmaskedPart;
        }

        public static string Maskify1(string cc)
        {
            return cc?.Substring(cc.Length < 4 ? 0 : cc.Length - 4).PadLeft(cc.Length, '#');
        }
        //take first 5 chars --- string str = yourStringVariable.Substring(0,5);

        //public static List<int> Fib(int length)
        //{
        //    var fib = 0;
        //    if (fib == 0)
        //    {

        //    }

        //    return new List<int>() {
        //        1,2,3
        //    }
        //    ;
        //}

        // fib first attempt
        public static List<int> FibList(int length)
        {
            var listOfNums = new List<int>();
            if (length <= 0) return listOfNums;

            if (length == 1)
            {
                listOfNums.Add(0);
                return listOfNums;
            }

            if (length == 2)
            {
                listOfNums.Add(0);
                listOfNums.Add(1);
                return listOfNums;
            }

            listOfNums = new List<int> { 0, 1 };
            var n = 3;
            while (length > 2)
            {
                listOfNums.Add(listOfNums[n - 1] + listOfNums[n - 2]);
                n++;
                length--;
            }

            return listOfNums;
        }

        //public static List<int> FibListRecursive(int length)
        //{
        //    var listOfFib = new List<int>();
        //    var i = 0;
        //    while (length > 0)
        //    {
        //        listOfFib.Add(GenFib(i));
        //        i++;
        //        length--;
        //    }

        //    return listOfFib;
        //}


        public static decimal GenFib(decimal n, Dictionary<decimal, decimal> memoisationDict = null)
        {
            memoisationDict = memoisationDict ?? new Dictionary<decimal, decimal>();
            decimal result;
            if (n == decimal.Zero) return Decimal.Zero;
            if (n == 1 || n == 2) return Decimal.One;
            try
            {
                return memoisationDict[n];
            }
            catch (Exception)
            {
                result = GenFib(n - 1, memoisationDict) + GenFib(n - 2, memoisationDict);
                memoisationDict.Add(n, result);
                return result;
            }
        }


        public static ulong[] productFib(ulong prod)
        {
            decimal n = 0;
            while (true)
            {
                var element1 = GenFib(n);
                var element2 = (decimal)GenFib(n + 1);
                var multiple = element1 * element2;
                if (multiple == prod) return new ulong[] { (ulong)element1, (ulong)element2, 1 };
                if (multiple > prod) return new ulong[] { (ulong)element1, (ulong)element2, 0 };
                n++;
            }
        }
    }
}