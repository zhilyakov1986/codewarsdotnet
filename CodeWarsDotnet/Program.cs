﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CodeWarsDotnet
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            // Console.WriteLine(CodeWarsEasy2.Animals(779, 988));
            // Console.WriteLine(Codewars8Kyu.GenerateRange(2,20,2));
            Console.WriteLine(Codewars8Kyu.FirstNonConsecutive(new int[] { 1, 2, 3, 4, 6, 7, 8 }));

        }
    }

    public static class Codewars8Kyu
    {
        public static bool SetAlarm(bool employed, bool vacation) => (employed && !vacation);
        
        public static object FirstNonConsecutive(int[] arr)
        {
            for (var i = 0; i < arr.Length-1; i++)
            {
                if (arr[i + 1] - arr[i]!= 1)
                {
                    return arr[i+1];
                }
            }

            return null;

        }
        public static string Interpret(string code)
        {
            switch (code)
            {
                case "H":
                    return "Hello World!";

                case "Q":
                    return code;

                case "9":
                    return "99";

                default:
                    return null;
            }
        }

        public static int[] GenerateRange(int min, int max, int step)
        {
            var result = new List<int>();
            for (var i = min; i <= max; i += step)
            {
                result.Add(i);
            }

            return result.ToArray();
        }
    }
}