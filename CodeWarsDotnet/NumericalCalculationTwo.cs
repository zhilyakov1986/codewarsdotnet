﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CodeWarsDotnet
{
    public class NumericalCalculationTwo
    {
        public static int FinalGrade(int exam, int projects)
        {

            if (exam > 90 || projects > 10)
                return 100;
            if (exam > 75 && projects >= 5)
                return 90;
            if (exam > 50 && projects >= 2)
                return 75;

            return 0;
        }

        public static bool PowerOfTwo(int n)
        {
            if (n == 0)
            {
                return false;
            }

            var log2 = (decimal)Math.Log(n, 2);
            var result = decimal.Round(log2, 2) % 1 == 0;
            return result;
        }

        public static bool PowerOfTwo2(int n)
        {
            return Convert.ToString(n, 2).Count(ch => ch == '1') == 1;
        }

        public static string GenerateShape(int n)
        {
            var result = "";
            if (n > 1)
            {
                var count = n;
                while (count > 0)
                {
                    count--;
                    if (count == 0)
                    {
                        result = result + new string('+', n);
                    }
                    else
                    {
                        result = result + new string('+', n) + '\n';
                    }

                }

            }

            if (n == 1)
            {
                result = "+";
            }

            return result;
        }

        public static string GenerateShape2(int n)
            => string.Join("\n", Enumerable.Repeat(new string('+', n), n));

        public static bool IsLeapYear(int year)
        {
            if (year % 4 != 0) return false;
            if (year % 100 != 0) return true;
            return year % 400 == 0;
        }

        public static bool IsLeapYear2(int year)
        {
            return DateTime.IsLeapYear(year);
        }


        //my first factorial
        public static int factorial(int n)
        {
            switch (n)
            {
                case 0:
                case 1:
                    return 1;
                default:
                    return n * factorial(n - 1);
            }
        }

        public static int factorial2(int n) => (n == 0) ? 1 : n * factorial2(n - 1);

        public static int CountArgs(params object[] args)
        {
            return args.Length;
        }
        public static int sumTwoSmallestNumbers(int[] numbers)
        {
            var listNum = new List<int>(numbers);
            listNum.Sort();
            return listNum[0] + listNum[1];

        }

        public static int sumTwoSmallestNumbersLinq(int[] numbers)
        {
            return numbers.OrderBy(i => i).Take(2).Sum();
        }

        public static long MaxRot(long n)
        {
            // define the list of numbers and put the original number in it
            var numList = new List<long>() { n };
            //create the list of chars from that original number
            var numCharList = new List<char>();
            numCharList.AddRange(n.ToString());


            for (int i = 0; i < numCharList.Count - 1; i++)
            {
                var removedItem = numCharList[i];
                numCharList.RemoveAt(i);
                numCharList.Add(removedItem);

                //cast char list to string and then to long and add it to the list
                var numberResult = long.Parse(string.Concat(numCharList));
                numList.Add(numberResult);
            }


            return numList.Max();
        }
    }
}