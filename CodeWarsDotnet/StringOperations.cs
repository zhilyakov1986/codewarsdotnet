﻿using System.Collections.Generic;

namespace CodeWarsDotnet
{
    internal class StringOperations
    {
        public static string ReverseWords(string str)
        {
            var splitString = str.Split(' ');
            var reversedOrderList = new List<string>();
            for (var i = splitString.Length - 1; i >= 0; i--) reversedOrderList.Add(splitString[i]);

            var joinList = string.Join(" ", reversedOrderList);

            return joinList;
        }

        public static char get_char(int number)
        {

            return (char)number;
        }

        public static string PadString(int value)
        {
            var num = value.ToString().PadLeft(5,'0');
            return "Value is " + num;
        }

        public static string PadString2(int value)
        {
            return $"Value is {value:D5}";
        }
    }
}