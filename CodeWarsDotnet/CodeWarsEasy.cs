﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CodeWarsDotnet
{
    public static class CodeWarsEasy
    {
        public static string HowMuchILoveYou(int nb_petals)
        {
            // your code
            var words = new List<string>() { "I love you", "a little", "a lot", "passionately", "madly", "not at all" };
            var choice = nb_petals % 6;
            return words[choice];
        }

        public static string HowMuchILoveYouSwitch(int nb_petals)
        {
            switch (nb_petals % 6)
            {
                case 5:
                    return "madly";
                case 1:
                    return "I love you";
                case 2:
                    return "a little";
                case 3:
                    return "a lot";
                case 4:
                    return "passionately";
                case 0:
                    return "not at all";
            }

            return "null";
        }


        public static int ToBinary(int n)
        {
            var binary = Convert.ToString(n, 2);
            return int.Parse(binary);
        }

        public static int ToBinary1(int n)
        {
            int q = n, tmp = 0, m = 1;
            while (q != 0)
            {
                tmp += q % 2 * m;
                q /= 2;
                m *= 10;
            }

            return tmp;
        }

        public static bool is_uppercase(this string phrase)
        {
            return string.Equals(phrase, phrase.ToUpper());
        }

        public static int RentalCarCost(int d)
        {
            if (d < 7)
            {
                return d * 40 - 20;
            }

            if (d >= 7)
            {
                return d * 40 - 50 - 20;
            }

            return d * 40;
        }

        public static int[] ReverseSeq(int n)
        {
            var result = new List<int>();
            for (var i = n; i >= 1; i--)
            {
                result.Add(i);
            }

            return result.ToArray();
        }

        public static int[] ReverseSeq1(int n)
        {
            return Enumerable.Range(1, n).Reverse().ToArray();
        }

        public static double basicOp(char operation, double value1, double value2)
        {
            switch (operation)
            {
                case '+':
                    return value1 + value2;
                case '-':
                    return value1 - value2;
                case '*':
                    return value1 * value2;
                case '/':
                    return value1 / value2;
            }

            return -1;
        }

        public static bool Check(object[] a, object x)
        {
            return a.Contains(x);
        }

        public static int[] InvertValues(int[] input)
        {
            return input.Select(i => -i).ToArray();
        }

        public static int TwiceAsOld(int dadYears, int sonYears)
        {
            // Add you code here.
            return Math.Abs(dadYears - sonYears * 2);
        }

        public static string HowManyDalmatians(int n)
        {
            List<string> dogs = new List<string>()
            {
                "Hardly any",
                "More than a handful!",
                "Woah that's a lot of dogs!",
                "101 DALMATIONS!!!"
            };
            if (n <= 10)
            {
                return dogs[0];
            }

            if (n <= 50 && n > 10)
            {
                return dogs[1];
            }

            return n == 101 ? dogs[3] : dogs[2];
        }

        public static string HowManyDalmatians2(int n)
        {
            List<string> dogs = new List<string>()
            {
                "Hardly any",
                "More than a handful!",
                "Woah that's a lot of dogs!",
                "101 DALMATIONS!!!"
            };
            string respond = n <= 10
                ? dogs[0]
                : (n <= 50
                    ? dogs[1]
                    : (n == 101
                        ? dogs[3]
                        : dogs[2]));
            return respond;
        }

        public static bool None(int[] arr, Func<int, bool> fun)
        {
            return arr.Length == 0 || arr.All(i => !fun(i));
        }

        public static bool None1(int[] arr, Func<int, bool> fun)
        {
            return !arr.Any(fun);
        }

        public static bool Any(int[] arr, Func<int, bool> fun)
        {
            return arr.Any(fun);
        }

        public static bool CorrectTail(string body, string tail)
        {
            return body.Substring(body.Length - 1) == tail;
        }

        public static bool CorrectTail1(string body, string tail)
        {
            return body.EndsWith(tail);
        }

        public static string Problem(String a)
        {
            //magic code 
            try
            {
                var num = double.Parse(a);
                return (num * 50 + 6).ToString();
            }
            catch (Exception)
            {
                return "Error";
            }
        }

        public static bool Include(int[] arr, int item)
        {
            return arr.Contains(item);
        }

        public static int CockroachSpeed(double x)
        {
            return (int)Math.Floor(x * 100000 / 36000);
        }

        public static bool IsDigit(string s)
        {
            var trimmedS = s.Trim();
            return double.TryParse(trimmedS, out _);

        }

        public static int GetAverage(int[] marks)
        {
            return (int)Math.Round((double)(marks.Sum() / marks.Length));
        }

        public static int GetAverage1(int[] marks)
        {
            return (int)marks.Average();
        }

        //public static string ChromosomeCheck(string s) => $"Congratulations! You're going to have a {s == "XY" ? "son" : "daughter"}.";
        public class Human
        {
        }
        public class Man : Human
        {
        }

        public class Woman : Human
        {
        }

        public static Human[] Create()
        {
            var human = new Human();
            var men = new Man();
            var woman = new Woman();

            var humans = new Human[] { men, woman };
            return humans;
        }

        //public class God
        //{
        //    public static Human[] Create() => new Human[] { new Man(), new Woman() };
        //}
        //public class Human { }
        //public class Man : Human { }
        //public class Woman : Human { }

        public static string[] StringToArray(string str)
        {
            // code code code
            return str.Split(' ');
        }

        public static int[] distinct(int[] a)
        {
            return new HashSet<int>(a).ToArray();
        }

        public static int[] distinct2(int[] a)
        {
            return a.Distinct().ToArray();
        }
    }
}